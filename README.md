# dock
A simple Graphic UI to run Docker

![](https://i.imgur.com/moQwrXh.png)

#### Requirements

* Zenity (sudo apt-get install zenity )
* Docker 
* leafpad (for option logs)
* git or wget


#### Install with Git

```
cd /tmp; git clone https://gitlab.com/0x1/dock; cd dock
mv dock /usr/bin/dock; cd; rm -rf /tmp/dock
chmod +x /usr/bin/dock
```

#### Install with Wget

```
wget https://gitlab.com/0x1/dock/raw/master/dock
mv dock /usr/bin/dock
chmod +x /usr/bin/dock
```

![](https://i.imgur.com/jXOC4Vk.png)
